All transcriptions of primary source materials, particularly the manuscripts, have been prepared using the [Transkribus](https://readcoop.eu/transkribus) software package. This system allows us to upload page images, then split them into the various parts of a page, tag them, and enter rich transcriptions (with information about strikethroughs, insertions, and so forth). These transcriptions can then be further tagged and annotated.

This page describes our basic process for those transcriptions, inspired largely by [Transkribus's own best practices document](https://readcoop.eu/transkribus/howto/transkribus-transcription-conventions/) and the collection of excellent blog posts provided by the [Rechtsprechung im Ostseeraum project.](https://rechtsprechung-im-ostseeraum.archiv.uni-greifswald.de/category/transkribus-in-practice/)

# Image Preparation

Images were sourced as follows:

- Photographs of Weldon's ToI manuscript: taken by Charles H. Pence in 2017, UCL Special Collections

Those images were then further processed using [ScanTailor](https://scantailor.org/), which allows for splitting by page, trimming of extra parts of the image, and rotation of text to fix skewed images.

# Transcription

After being loaded into Transkribus, page layout was automatically detected using the basic Transkribus layout detection model.

The transcription process itself proceeds as follows:

1. Detected text baselines are cleaned up; small or incorrect baselines are deleted or merged.

2. The default model creates one single text region object covering all text detected on the page. This region is split, to create:

   - a separate, small region containing only the page number (if any)
     - the line within this region (not the region itself) is tagged with the "page-number" structural tag
   - a separate region containing page headers or chapter headings (if any)
     - the line(s) within this region (not the region itself) is tagged with the "heading" structural tag
   - the main text region is split into as many regions as their are paragraphs, and each of those text regions is tagged with the "paragraph" structural tag
   - separate regions are created for each instance of marginalia (that is, text clearly added _after_ the preparation of the document, whether by Weldon or by another author such as Karl Pearson)

3. Set the "reading order" of the regions correctly. (This may have been altered, especially if a page-number region was added at the top of the page, for instance.) The easiest way to edit the reading order is to open the "Layout" tab on the menu, select the different region or line objects, and set their order using the up- and down-arrow buttons above the list. The reading order is:

   - page number (if at top of page)
   - header
   - paragraphs/sections, moving from top-left to bottom-right in reading order
   - marginalia
   - page number (if at bottom of page)

4. If there are inter-line insertions, each insertion should be a separate baseline. The reading order of those insertions should be edited, however, so that the line containing the insertion comes first, followed by the insertion(s), followed by the next full line of text.

5. The last line of each paragraph (i.e., the line which terminates with a proper paragraph break) should be selected, and the "toggle paragraph" button should be clicked in the toolbar below the transcription window. (To make it easier to see if you've done this, click the "Transcription settings" button, at the far right in the toolbar, and enable "Show control signs.") Make sure to double-check these when paragraph breaks might happen at the last line on a page.

6. Once all text regions and baselines are correct and tagged, input the transcription in the transcription window, following the below conventions.

# Text Conventions

These transcriptions are not the place to correct or modernize spelling, punctuation conventions, etc. Text should be entered just as it appears on the page. This will often lead, e.g., to spaces before semicolons, occasional strange capitalization, and so forth. This is expected. If there is a period, for instance, at the end of a chapter heading, include it ("1. — Introductory."). Attempt to accurately represent inserted punctuation as well (em and en dashes, other markings) using the appropriate Unicode characters.

Text that is added _purely_ by the archivist and is _only_ of archival significance (e.g., the archives' recto/verso folio pagination scheme) is _not_ included. There's a bit of judgment call here. For instance, on the title page of the first chapter of the _Theory of Inheritance_ manuscript, there is a large "A" next to "Chapter 1," presumably added by an archivist to distinguish the multiple versions of the first chapter. This is included, as it is helpful for disambiguation. But there is also a "22 + 7" written there, indicating how many pages are found in the two versions of the chapter, and the mark "f. 1" in the bottom-right corner. These are not transcribed.

Weldon has a habit of using median-points (·) rather than decimals (.) in decimal numbers. They should be entered as median-points, again, as they appear in the text. Equations and fractions should be entered as closest-approximation Unicode symbols. If an simple over/under fraction is used, generate it correctly using the Unicode fraction tools, for example with: <https://lights0123.com/fractions/>.

## Text Tagging

- Underlining should be represented as underline (the "underline" button just below the text editor), not as italics. More generally, attempt to include any and all formatting just as it was written on the page by the author.

- If a section of text has been struck through or blackened by the author, and the original can still be deciphered, type the original text in the transcription, then select the struck region, and mark it as strikethrough (the "strike-through" button just below the text editor).

- If you can _guess_ the content under the strikethrough, but you aren't sure about it, do the same thing as above, but _also_ select the text and add the "unclear" tag.

- If text has been _completely_ blackened, to the point of being unreadable, add one extra space at the location where the text has been blackened. Click _between_ the two spaces (e.g., "word (here) word"), and at that location, add the "gap" tag, indicating a gap in the transcription.

- The same "gap" tag as in the previous point should be used if a space was obviously and deliberately left at a point in the manuscript (e.g., if it looks like the intent was to come back and write something in later).

- (As an aside: Do _not_ use the "blackening" tag. This is for the redaction of sensitive information in published transcriptions, and has nothing to do with our use-case.)

- If the text is in a marginalia region, and the author of the marginalia can be detected, select the entire transcription of the given line, and add a "comment" tag of the form "Added by (name)". If the author of the marginalia is unclear, add a question mark at the end of the comment (e.g., "Added by archivist?").

- All inter-line insertions and marginalia should be marked with the "add" tag.

- Do not expand abbreviations when transcribing. Select the abbreviation and tag it with the "abbreviation" tag.

# Transcription Editing

As soon as the initial steps described above have been performed and double-checked, the status of the transcribed page should be toggled from "In Progress" to "Done". This is the indication that the initial transcription has been completed and is ready for review.

Each transcription is then carefully compared against the image.

# Post-Tagging

(FIXME: We haven't actually done this yet, so I may change my mind about it when it comes time to actually _do_ the thing!)

Transkribus supports the tagging of personal names, places, organizations, dates, and "works" (references to other written work).