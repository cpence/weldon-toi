Welcome to project documentation for the Weldon _Theory of Inheritance_ digital
edition.. We keep here shareable, open-access information about how we prepared
(and are preparing) the edition.

- [Transcription](transcription.md)
